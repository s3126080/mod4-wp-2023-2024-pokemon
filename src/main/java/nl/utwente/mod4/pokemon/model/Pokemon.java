package nl.utwente.mod4.pokemon.model;

public class Pokemon extends NamedEntity {
    public int pokemonType;
    public int trainerId;

    public Pokemon() {
        super();
        pokemonType = 0;
        trainerId = 0;
    }
}
