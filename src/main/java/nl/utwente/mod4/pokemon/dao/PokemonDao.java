package nl.utwente.mod4.pokemon.dao;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.*;
import nl.utwente.mod4.pokemon.Utils;
import nl.utwente.mod4.pokemon.model.Pokemon;
import nl.utwente.mod4.pokemon.model.Trainer;

public enum PokemonDao {

    INSTANCE;

    private static final String ORIGINAL_POKEMON = Utils.getAbsolutePathToResources() + "/default-pokemon-dataset.json";
    private static final String POKEMON = Utils.getAbsolutePathToResources() + "/pokemon.json";

    private HashMap<String, Pokemon> pokemons = new HashMap<>();

    public void delete(String id) {
        if (pokemons.containsKey(id)) {
            pokemons.remove(id);
        } else {
            throw new NotFoundException("Pokemon '" + id + "' not found.");
        }
    }

    public List<Pokemon> getPokemon(int pageSize, int pageNumber) {
        var list = new ArrayList<>(pokemons.values());
        return (List<Pokemon>) Utils.pageSlice(list, pageSize, pageNumber);
    }

    public Pokemon getPokemon(String id) {
        var pt = pokemons.get(id);

        if (pt == null) {
            throw new NotFoundException("Pokemon '" + id + "' not found!");
        }

        return pt;
    }

    public void load() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File source = existsTrainers() ?
                new File(POKEMON) :
                new File(ORIGINAL_POKEMON);
        Pokemon[] arr = mapper.readValue(source, Pokemon[].class);

        Arrays.stream(arr).forEach(pokemon -> pokemons.put(pokemon.id, pokemon));
    }

    public void save() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        File destination = new File(POKEMON);

        writer.writeValue(destination, pokemons.values());
    }

    private boolean existsTrainers() {
        File f = new File(POKEMON);
        return f.exists() && !f.isDirectory();
    }

    private int getMaxId() {
        Set<String> ids = pokemons.keySet();
        return ids.isEmpty() ? 0 : ids.stream()
                .map(Integer::parseInt)
                .max(Integer::compareTo)
                .get();
    }


    public Pokemon create(Pokemon newPokemon) {
        String nextId = "" + (getMaxId() + 1);

        newPokemon.id = nextId;
        newPokemon.created = Instant.now().toString();
        newPokemon.lastUpDate = Instant.now().toString();
        pokemons.put(nextId, newPokemon);

        return newPokemon;
    }

    public Pokemon replace(Pokemon updated) {
        if(!updated.checkIsValid())
            throw new BadRequestException("Invalid pokemon.");
        if(pokemons.get(updated.id) == null)
            throw new NotFoundException("Pokemon id '" + updated.id + "' not found.");

        updated.lastUpDate = Instant.now().toString();
        pokemons.put(updated.id, updated);

        return updated;
    }

    public int getTotalPokemon() {
        return pokemons.keySet().size();
    }
}
