package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Comparator;
import nl.utwente.mod4.pokemon.dao.PokemonDao;
import nl.utwente.mod4.pokemon.model.Pokemon;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

@Path("/pokemon")
public class PokemonRoute {

    // Gets all the available pokemon and sorts them, if specified.
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<Pokemon> getAvailablePokemon(@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber, @QueryParam("sortBy") String sortType) {
        int ps = pageSize > 0 ? pageSize : Integer.MAX_VALUE;
        int pn = pageNumber > 0 ? pageNumber : 1;

        Pokemon[] resources = PokemonDao.INSTANCE.getPokemon(ps, pn).toArray(new Pokemon[0]);


        int total = PokemonDao.INSTANCE.getTotalPokemon();
        ps = resources == null ? 0 : resources.length;
        if (sortType != null && sortType.equals("name")) {
            resources = Arrays.stream(resources).sorted(Comparator.comparing(p -> p.name))
                    .toArray(Pokemon[]::new);
        }

        return new ResourceCollection<>(resources, ps, pn, total);
    }

    // Gets an individual pokemon with the specified id.
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Pokemon getPokemon(@PathParam("id") String id) {
        // returns to the client the resource with the desired id
        Pokemon poke;
        try {
            poke = PokemonDao.INSTANCE.getPokemon(id);
        } catch (NotFoundException e) {
            throw new WebApplicationException(404);
        }
        return poke;
    }

    // Deletes a pokemon with the specified id.
    @DELETE
    @Path("/{id}")
    public void deletePokemon(@PathParam("id") String id) {
        PokemonDao.INSTANCE.delete(id);
    }

    // Updates a pokemon with the specified id.
    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pokemon updatePokemon(@PathParam("id") String id, Pokemon poke) {
        if (id == null || !id.equals(poke.id)) {
            throw new BadRequestException("Id mismatch.");
        }
        return PokemonDao.INSTANCE.replace(poke);
    }

    // Creates a new pokemon
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pokemon addPokemon(Pokemon poke) {
        if (poke.id != null || poke.created != null || poke.lastUpDate != null) {
            throw new BadRequestException("Invalid data provided.");
        }
        return PokemonDao.INSTANCE.create(poke);
    }
}
